import React, { Component } from 'react';
import {View, Text } from 'react-native';
import { Card } from 'react-native-elements';


class Contact extends Component {

    static navigationOptions = {
        title: 'Contact'
    };

    render() {
        return(
           
			 <View>

			 	<Card >
			 		<Card.Title>Contact Information</Card.Title>
			 		<Card.Divider/>
                    <Text h1>121, Clear Water Bay Road </Text>
                     	<Text h1>Clear Water Bay, Kowloon </Text>
						<Text h1> HONG KONG</Text>
						<Text h1>Tel: +852 1234 5678 </Text>
						<Text h1>Fax: +852 8765 4321 </Text>
						<Text h1>Email:confusion@food.net </Text>
					
                </Card>
              </View>);
    }
}

export default Contact;
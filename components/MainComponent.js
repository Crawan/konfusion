import React, { Component } from 'react';
import Menu from './MenuComponent';
import  Dishdetail from './DishdetailComponent';
import  Contact from './ContactComponent';
import  AboutUs from './AboutusComponent';
import { View, Platform } from 'react-native';
//import { createStackNavigator } from 'react-navigation-stack';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Home from './HomeComponent';
import { Icon } from 'react-native-elements';

const AboutUstNavigator = createStackNavigator();
function AboutUsNavigatorScreen(){
     return(
        <AboutUstNavigator.Navigator
            initialRouteName='About Us'
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: "#fff"            
                }
            }}
        >  
          <AboutUstNavigator.Screen
                name="About Us"
                component={AboutUs}
            />                    
        </AboutUstNavigator.Navigator>
    );
}


const ContactNavigator = createStackNavigator();
function ContactNavigatorScreen(){
     return(
        <ContactNavigator.Navigator
            initialRouteName='Contact Us'
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: "#fff"            
                }
            }}
        >  
          <ContactNavigator.Screen
                name="Contact"
                component={Contact}
            />                    
        </ContactNavigator.Navigator>
    );
}

const MenuNavigator = createStackNavigator();
function MenuNavigatorScreen() {
    return(
        <MenuNavigator.Navigator
            initialRouteName='Menu'
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: "#fff"            
                }
            }}
        >
            <MenuNavigator.Screen
                name="Menu"
                component={Menu}
            />
            <MenuNavigator.Screen
                name="Dishdetail"
                component={Dishdetail}
                options={{ headerTitle: "Dish Detail"}}
            />            
        </MenuNavigator.Navigator>
    );
}

const HomeNavigator = createStackNavigator();

function HomeNavigatorScreen(){
     return(
        <HomeNavigator.Navigator
            initialRouteName='Home'
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: "#fff"            
                }
            }}
        >  
          <HomeNavigator.Screen
                name="Home"
                component={Home}
            />                    
        </HomeNavigator.Navigator>
    );
}

const MainNavigator = createDrawerNavigator();
class Main extends Component {
    render() {

        return (
          <NavigationContainer>
              <MainNavigator.Navigator initialRouteName="Home">
                <MainNavigator.Screen name="Home" component={HomeNavigatorScreen} />
                <MainNavigator.Screen name="About Us" component={AboutUsNavigatorScreen} />
                <MainNavigator.Screen name="Menu" component={MenuNavigatorScreen} />
                <MainNavigator.Screen name="Contact Us" component={ContactNavigatorScreen} />
              </MainNavigator.Navigator>
         </NavigationContainer>

        );
    }
}

export default Main;